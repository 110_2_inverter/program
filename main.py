#-*- coding: utf-8 -*-

#引用Library
from serial import Serial
from modbus_tk.modbus_rtu import RtuMaster
from modbus_tk.modbus_tcp import TcpMaster
import datetime
import sys
import traceback

def ParseException(Exceptions):
    error_class = Exceptions.__class__.__name__
    detail = Exceptions.args[0]
    cl , exc , tb = sys.exc_info()
    lastCallStack = traceback.extract_tb(tb)[-1]
    fileName = lastCallStack[0].split('\\')[-1]
    lineNum = lastCallStack[1]
    funcName = lastCallStack[2]
    errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
    print(errMsg)

def modbus_03(master:RtuMaster,slaveID:int,starting_address:int,quantity_of_x:int)->list:
    try:
        return list(master.execute(slaveID,0x03,starting_address,quantity_of_x))
    except Exception as e:
        ParseException(e)
        return []
        
def modbus_10(master:RtuMaster,slaveID:int,starting_address:int,output_value:list)->bool:
    try:
        master.execute(slaveID,0x10,starting_address,output_value=output_value)
        return True
    except Exception as e:
        ParseException(e)
        return False
    
def dataTypetoData(data: list[int],datatype: str):
    if len(data):
        if datatype == "INT16U":
            return data[0]
        elif datatype == "INT16":
            return data[0] if '{:016b}'.format(data[0])[0] == "0" else -1*(data[0]-1)^0xFFFF
        elif datatype == "INT32U":
            return (data[0] << 16) + data[1]
        else:
            print(f"dataTypetoData : type {datatype} not supported")
    return None

def multiOffsetData(data:float,div:float,mul:float,offset:float)->float:
    if div == 0:
        print("multiOffsetData : div cannot be zero")
        return None
    else:
        return round(data/div*mul+offset,2)
    
def processData(data:list,equipment:dict,index:int)->dict:
    #equipment['content'] => [資料名稱,地址,資料型態,除法,乘法,位移]
    if len(data):
        return_dict = {}
        starting_address = equipment['content'][equipment['start_num'][index]][1]
        for i in equipment['content'][equipment['start_num'][index]:]:
            while True:
                if not len(data):
                    break
                if starting_address == i[1]:
                    temp = []
                    if i[2] in ["INT16","INT16U"]:
                        temp.append(data.pop(0))
                        return_dict[i[0]] = multiOffsetData(dataTypetoData(temp,i[2]),i[3],i[4],i[5])
                        starting_address += 1
                    elif i[2] in ["INT32U"]:
                        temp.append(data.pop(0))
                        temp.append(data.pop(0))
                        return_dict[i[0]] = multiOffsetData(dataTypetoData(temp,i[2]),i[3],i[4],i[5])
                        starting_address += 2
                    else:
                        print(f"processData : type {i[2]} not found")
                        return_dict[i[0]] = None
                        starting_address += 1
                    break
                else:
                    try:
                        data.pop(0)
                        starting_address += 1
                    except Exception as e:
                        print(e)
                        break
        return return_dict
    else:
        print("processData : no data found")
        return {}

def readData(master:RtuMaster,slaveID:int,equipment:dict)->dict:
    read_dict = {
        "time": datetime.datetime.now(),
        "ID" : f"inverter{slaveID}"
    }
    try:
        for i in range(len(equipment['read_time'])):
            read_len = int(equipment['read_time'][i])
            start_address = int(equipment['content'][equipment['start_num'][i]][1])
            response = modbus_03(master,slaveID,start_address,read_len)
            response = processData(response,equipment,i)
            read_dict = {**read_dict,**response}
        return read_dict
    except Exception as e:
        ParseException(e)
        return {}
             
if __name__ == '__main__':
    equipment = {
        "read_time":[6,20,7],
        "start_num":[0,3,22],
        "content":[
            ["kwh",0x1652,"INT32U",100,1,0],
            ["money",0x1654,"INT32U",100,1,0],
            ["co2",0x1656,"INT32U",100,1,0],
            ["kva",0x168E,"INT32U",1000,1,0],
            ["pv_1_v",0x1690,"INT16U",10,1,0],
            ["pv_1_i",0x1691,"INT16U",10,1,0],
            ["pv_2_v",0x1692,"INT16U",10,1,0],
            ["pv_2_i",0x1693,"INT16U",10,1,0],
            ["pv_3_v",0x1694,"INT16U",10,1,0],
            ["pv_3_i",0x1695,"INT16U",10,1,0],
            ["pv_4_v",0x1696,"INT16U",10,1,0],
            ["pv_4_i",0x1697,"INT16U",10,1,0],
            ["v_bus_1",0x1698,"INT16U",10,1,0],
            ["i_bus_1",0x1699,"INT16U",10,1,0],
            ["v_bus_2",0x169A,"INT16U",10,1,0],
            ["i_bus_2",0x169B,"INT16U",10,1,0],
            ["v_bus_3",0x169C,"INT16U",10,1,0],
            ["i_bus_3",0x169D,"INT16U",10,1,0],
            ["v_bus",0x169E,"INT16U",10,1,0],
            ["i_bus",0x169F,"INT16U",10,1,0],
            ["f",0x16A0,"INT16U",100,1,0],
            ["pf",0x16A1,"INT16",1000,1,0],
            ["p_cell_total",0x16A2,"INT32U",1000,1,0],
            ["p_bus_total",0x16A4,"INT32U",1000,1,0],
            ["temp_1",0x16A6,"INT16",100,1,0],
            ["temp_2",0x16A7,"INT16",100,1,0],
            ["temp_3",0x16A8,"INT16",100,1,0],
        ]
    }
    
    master = TcpMaster(
        host="localhost",
        port=502
    )
    # master = RtuMaster(Serial(
    #     port = "COM1",
    #     baudrate = 9600,
    #     bytesize = 8,
    #     parity = "N",
    #     stopbits = 1
    # ))
    master.set_timeout(2)
    
    data = readData(master,1,equipment)
    for key,value in data.items():
        print(f"{key} : {value}")
    
