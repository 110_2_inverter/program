## 安裝python套件
	pip install modbus_tk pyserial

## 引用套件

```python
from serial import Serial
from modbus_tk.modbus_rtu import RtuMaster
from modbus_tk.modbus_tcp import TcpMaster
import datetime
import sys
import traceback
```

## 建立Modbus RTU/TCP Master
> Windows RTU Master設定
```python
master = RtuMaster(Serial(
	port = "COM1",
	baudrate = 9600,
	bytesize = 8,
	parity = "N",
	stopbits = 1
))
```

> Linux RTU Master設定
```python
master = RtuMaster(Serial(
	port = "/dev/ttyS0",
	baudrate = 9600,
	bytesize = 8,
	parity = "N",
	stopbits = 1
))
```

> Windows/Linux TCP設定
```python
master = TcpMaster(
	host="127.0.0.1",
	port=502
)
```

## 設定timeout

```python
#RTU無預設(無設定必定timeout)
#TCP預設5秒

master.set_timeout(1) # 單位:秒
```

## Modbus master 操作
>  Modbus 0x03 功能碼
```python
#讀取(功能碼0x03)
#功能：從暫存器0x168E開始讀取2個資料
#回傳：tuple (1,3)
#意義：0x168E:1 0x168F:3
data = master.execute(
	slave=1,
	function_code=0x03,
	starting_address=0x168E,
	quantity_of_x=2
)
```
> Modbus 0x10 功能碼
```python
#寫入(功能碼0x10)
#功能：將0xAAAA寫入暫存器0x1400
#回傳：tuple (5120,1)
#意義：從0x1400(5120)寫入 長度為1
data = master.execute(
	slave=1,
	function_code=0x10,
	starting_address=0x1400,
	output_value=[0xAAAA]
)
```

## 處理回傳資料

|資料型態|縮寫|
| ------------ | ------------ |
|16-bit unsigned integer|INT16U|
|16-bit signed integer|INT16|
|32-bit unsigned integer|INT32U|


> 將Modbus回傳資料轉換成 INT
```python
def dataTypetoData(data:list[int],datatype:str):
    if len(data):
        if datatype == "INT16U":
            return data[0]
        elif datatype == "INT16":
            return data[0] if '{:016b}'.format(data[0])[0] == "0" else -1*(data[0]-1)^0xFFFF
        elif datatype == "INT32U":
            return (data[0] << 16) + data[1]
        else:
            print(f"dataTypetoData : type {datatype} not supported")
    return None
```
> 將回傳資料進行放大、縮小、位移
```python
def multiOffsetData(data:float,div:float,mul:float,offset:float)->float:
    if div == 0:
        print("multiOffsetData : div cannot be zero")
        return None
    else:
        return round(data/div*mul+offset,2)
```

## 建立連續讀取資料表

|資料名稱|地址|資料型態|除法|乘法|位移|
| ------------ | ------------ | ------------ | ------------ | ------------ | ------------ |
|pv_1_v|0x1690|INT16U|10|1|0|
|pv_1_i|0x1691|INT16U|10|1|0|
|temp_1|0x16A6|INT16|100|1|0|
|temp_2|0x16A7|INT16|100|1|0|

```python
equipment = {
    "read_time": [2,1],
    "start_num": [0,2],
    "content": [
        ["pv_1_v",0x1690,"INT16U",10,1,0],
        ["pv_1_i",0x1691,"INT16U",10,1,0],
        ["temp_1",0x16A6,"INT16",100,1,0],
        ["temp_2",0x16A7,"INT16",100,1,0],
    ]
}
```

## 利用讀取資料表讀取

```python
data = master.execute(
	slave=1,
	function_code=0x03,
	starting_address=start_address = int(equipment['content'][ equipment['start_num'][0] ][1])
	quantity_of_x=int(equipment['read_time'][0])
)
```

## 處理資料
```python
def processData(data:list,equipment:dict,index:int)->dict:
    #equipment['content'] => [資料名稱,地址,資料型態,除法,乘法,位移]
    if len(data):
        return_dict = {}
        starting_address = equipment['content'][equipment['start_num'][index]][1]
        for i in equipment['content'][equipment['start_num'][index]:]:
            while True:
                if not len(data):
                    break
                if starting_address == i[1]:
                    temp = []
                    if i[2] in ["INT16","INT16U"]:
                        temp.append(data.pop(0))
                        return_dict[i[0]] = multiOffsetData(dataTypetoData(temp,i[2]),i[3],i[4],i[5])
                        starting_address += 1
                    elif i[2] in ["INT32U"]:
                        temp.append(data.pop(0))
                        temp.append(data.pop(0))
                        return_dict[i[0]] = multiOffsetData(dataTypetoData(temp,i[2]),i[3],i[4],i[5])
                        starting_address += 2
                    else:
                        print(f"processData : type {i[2]} not found")
                        return_dict[i[0]] = None
                        starting_address += 1
                    break
                else:
                    try:
                        data.pop(0)
                        starting_address += 1
                    except Exception as e:
                        print(e)
                        break
        return return_dict
    else:
        print("processData : no data found")
        return {}
```
