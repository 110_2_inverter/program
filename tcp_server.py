import modbus_tk.modbus_tcp as modbus_tcp
import signal
import sys

def sigint_handler(signal,frame):
    server.stop()
    sys.exit(0)

server = modbus_tcp.TcpServer(address="localhost", port=502)
slave = server.add_slave(1)
slave.add_block("Block1",0x03,0x1652,6)
slave.add_block("Block2",0x03,0x168E,20)
slave.add_block("Block3",0x03,0x16A2,7)
slave.set_values("Block1",0x1652,[i for i in range(6)])
slave.set_values("Block2",0x168E,[i for i in range(6,26)])
slave.set_values("Block3",0x16A2,[i for i in range(26,33)])

signal.signal(signal.SIGINT,sigint_handler)
server.start()